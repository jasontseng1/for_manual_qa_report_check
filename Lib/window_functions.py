# _*_ coding: UTF-8 _*_
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class WindowFunctions:
    """ 視窗函式 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def enter_to_new_window(self, old_window):
        """
        進入新視窗
        參數:
            old_window (str): 原始視窗id
        Returns:
            (bool): 進新視窗為 True，異常則是 False
        """

        # 判斷是否有餘額刷新彈窗出現，若出現代表無法進入遊戲
        try:
            WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(
                (By.XPATH, "//p[text()='餘額刷新中，請稍候']")))
            self.get_img("Fail：進入遊戲失敗，出現「餘額刷新中，請稍後」彈出視窗")
            WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(
                (By.XPATH, "//button[text()='確認']"))).click()
            return False

        except:
            all_windows = self.driver.window_handles
            if len(all_windows) > 1:
                for window in all_windows:
                    if window != old_window:
                        self.driver.switch_to.window(window)
                        return True
            else:
                self.driver.switch_to.window(old_window)
                self.get_img("Fail：無開啟新視窗")
                return False

    def back_to_window(self, back):
        """
        返回視窗頁面
        參數:
            back (str): 預計返回視窗id
        """

        all_windows = self.driver.window_handles
        for window in all_windows:
            if window != back:
                self.driver.switch_to.window(window)
                self.driver.close()
                self.driver.switch_to.window(back)
