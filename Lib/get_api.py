# _*_ coding: UTF-8 _*_
import requests
from pprint import pprint


class Api:
    """ 官網遊戲 api """

    @staticmethod
    def get_api_data(url):
        """
        取得 api 資料
        參數:
            url (str): api 網址

        Returns:
            (list|bool): 回傳 api 資料，api 異常則是 False
        """

        response = requests.get(url)
        if response.status_code == 200:
            response_data = response.json()
            return response_data
        else:
            print("Fail：api不通")
            print(f"api url：{url}")
            return False

    @staticmethod
    def check_w1_api_data(resp_data, platform_name):
        """
        確認 w1 健康度 api 資料
        參數:
            resp_data (list): api 回傳的資料
            platform_name (str): 平台名稱

        Returns:
            (bool): w1 api 健康度正常為 True，相反則是 False
        """

        # init
        result = True

        # 確認有無資料
        if platform_name not in [_['platform'] for _ in resp_data]:
            print(f"Fail：w1 api 無回傳「{platform_name}」資料，api資料如下")
            pprint(resp_data)
            result = False
        else:
            # 確認狀態，platform為遊戲商代碼，status為狀態：0正常、1緩慢、2逾時、3維護、其他為異常
            for i in resp_data:
                if i['platform'] == platform_name and i['status'] not in [0, 1]:
                    print("Fail：w1 api 目前非正常狀態")
                    print(f"{platform_name}，status狀態：{i['status']}")
                    result = False

        return result

    @staticmethod
    def check_h1_api_data(resp_data, switch_name):
        """
        確認 h1 遊戲開關 api 資料
        參數:
            resp_data (list): api 回傳的資料
            switch_name (str): 遊戲開關名稱

        Returns:
            (bool): h1 api 遊戲開關正常為 True，相反則是 False
        """

        # init
        result = True

        # 確認有無資料
        if switch_name not in [_['switchName'] for _ in resp_data]:
            print(f"Fail：h1 api 無回傳「{switch_name}」資料，api資料如下")
            pprint(resp_data)
            result = False
        else:
            # 確認狀態，switchName為開關名稱，status遊戲狀態：0關閉中、1啟用中
            for data in resp_data:
                if data['switchName'] == switch_name and data['status'] != '1':
                    print("Fail：h1 api 目前非正常狀態")
                    print(f"{switch_name}，status狀態：{data['status']}")
                    result = False

        return result

    @staticmethod
    def check_w1_and_h1_data(api_data, platform_name, switch_name):
        """
        確認 w1 健康度 及 h1 遊戲開關 api 資料
        參數:
            api_data (dict): api 資料
            platform_name (str): 平台名稱
            switch_name (str): 遊戲開關名稱

        Returns:
            (bool): api 皆正常為 True，相反則是 False
        """

        # 取得 w1 api 資料
        w1_resp = Api.get_api_data(api_data['w1'])
        if not w1_resp:
            return False

        # 確認 w1 健康度是否正常
        check_w1 = Api.check_w1_api_data(w1_resp, platform_name)
        if not check_w1:
            return False

        # 取得 h1 api 資料
        h1_resp = Api.get_api_data(api_data['h1'])
        if not h1_resp:
            return False

        # 確認 h1 遊戲開關是否有開啟
        check_h1 = Api.check_h1_api_data(h1_resp, switch_name)
        if not check_h1:
            return False

        return True
