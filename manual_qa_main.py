# _*_ coding: UTF-8 _*_
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import unittest
from Setting import *
from Lib import get_report
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from Live import Live_Type
from Slot import Slot_Type


options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()

# 視窗控制
window_dict = {
    'member_window': driver.current_window_handle,
    'manage_window': None,
    'super_manage_window': None
}


class ManualReport(unittest.TestCase):
    """ 協助手動報表 """

    live_ar = Live_Type.LiveAutoReport(driver, get_img, test_env, window_dict)
    slot_ar = Slot_Type.SlotAutoReport(driver, get_img, test_env, window_dict)

    @classmethod
    def setUpClass(cls) -> None:
        # 測試是否繼續變數
        cls.test_continue = True

    def setUp(self) -> None:
        """ 每個測試項目測試之前調用 """

        # 判斷測試是否繼續
        if not self.__class__.test_continue:
            self.skipTest("中斷測試")

    def test_argument(self):
        """ 確認參數 """

        # init
        result = True

        # 判斷測試環境(prod or uat)
        if test_env != "prod" and test_env != "uat":
            print(f"Fail：「test_env」參數錯誤，參數值：{test_env}")
            print("目前可接受參數值為：")
            print("參數值：uat，取「測試站」設定")
            print("參數值：prod，取「正式站」設定")
            self.__class__.test_continue = False
            result = False

        self.assertEqual(True, result)

    def test_login(self):
        """ 登入 """

        # init
        result_list = []
        driver.get(test_data['member_url'])
        time.sleep(2)

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(test_data['account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(test_data['account'][1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if test_data['account'][0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{test_data['account'][0]}，顯示帳號為:{check_name.text}")
                result_list.append("False")
                self.__class__.test_continue = False
            else:
                window_dict['member_window'] = driver.current_window_handle

            get_img("登入成功")
        except:
            get_img("登入失敗")
            result_list.append("False")
            self.__class__.test_continue = False

        if test_env == 'uat':
            # 設定視窗資訊
            try:
                get_manage_report = get_report.ManageReport(driver, get_img)
                # 登入代理端
                manage_check = get_manage_report.login()
                if manage_check:
                    window_dict['manage_window'] = driver.current_window_handle
                else:
                    result_list.append("False")
                    self.__class__.test_continue = False

                # 登入超帳
                super_manage_check = get_manage_report.super_login()
                if super_manage_check:
                    window_dict['super_manage_window'] = driver.current_window_handle
                else:
                    result_list.append("False")
                    self.__class__.test_continue = False

            except:
                if window_dict['manage_window'] is not None:
                    print("Fail：登入「代理」異常")
                elif window_dict['super_manage_window'] is not None:
                    print("Fail：登入「超帳」異常")
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        self.assertEqual(True, results)

    def test_live_type_report_now(self):
        """ 真人種類報表 - 當前 """

        result = self.live_ar.auto_report()
        self.assertEqual(True, result)

    def test_live_type_report_last(self):
        """ 真人種類報表 - 歷史 """

        result = self.live_ar.auto_report(now=False)
        self.assertEqual(True, result)

    def test_slot_type_report_now(self):
        """ 電子種類報表 - 當前 """

        result = self.slot_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_type_report_last(self):
        """ 電子種類報表 - 歷史 """

        result = self.slot_ar.auto_report(now=False)
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        ManualReport("test_argument"),
        ManualReport("test_login"),
        ManualReport("test_live_type_report_now"),
        ManualReport("test_live_type_report_last"),
        ManualReport("test_slot_type_report_now"),
        ManualReport("test_slot_type_report_last"),
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title='自動報表',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
