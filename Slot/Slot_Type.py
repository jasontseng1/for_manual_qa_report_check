# _*_ coding: UTF-8 _*_
from Lib import get_report


class SlotAutoReport:
    """ 電子自動報表 """

    def __init__(self, driver, get_img, test_env, window_data):
        self.driver = driver
        self.get_img = get_img
        self.test_env = test_env
        self.window_data = window_data

    def auto_report(self, now=True):
        """ 自動報表 """

        # init
        result_list = []
        manager_report = False
        manage_check_data = False
        super_report = False
        super_check_data = False

        # 切換至官網
        self.driver.switch_to.window(self.window_data['member_window'])
        self.driver.refresh()

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('3', '全部', search_now=now)
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report)
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        get_manage_report = get_report.ManageReport(self.driver, self.get_img)

        # 切換至代理端
        if self.window_data['manage_window'] is not None and self.test_env == 'uat':
            self.driver.switch_to.window(self.window_data['manage_window'])

            # 取得後台報表資料
            manager_report, page_data, page_check_result = get_manage_report.search_report('3', '全部', check_data, search_now=now)
            if not manager_report or not page_check_result:
                result_list.append("False")

            # 確認資料
            manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, check_now=now, check_type='3')
            if manage_check_data:
                # 整理遊戲名稱
                manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

                if not manage_check_data['確認結果']:
                    result_list.append("False")

        # 切換至超帳
        if self.window_data['super_manage_window'] is not None and self.test_env == 'uat':
            self.driver.switch_to.window(self.window_data['super_manage_window'])

            # 取得超帳報表資料
            super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('3', '全部', check_data, search_now=now)
            if not super_report or not super_page_check_result:
                result_list.append("False")

            # 確認資料
            super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, check_super=True, check_now=now, check_type='3')
            if super_check_data:
                # 整理遊戲名稱
                super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

                if not super_check_data['確認結果']:
                    result_list.append("False")

        if self.test_env == 'uat':
            # 比對官網與後台報表
            check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
            if not check_report_result:
                result_list.append("False")

            # 比對官網與超帳報表
            check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
            if not check_super_report_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
